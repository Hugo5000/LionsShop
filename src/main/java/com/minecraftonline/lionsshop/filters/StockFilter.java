package com.minecraftonline.lionsshop.filters;

import com.minecraftonline.lionsshop.LionsShopUtil;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.function.Predicate;

public class StockFilter implements Predicate<ItemStack> {
    private final int dmg;
    private final ItemType type;

    public StockFilter(ItemType type, int dmg) {
        this.dmg = dmg;
        this.type = type;
    }

    @Override
    public boolean test(ItemStack itemStack) {
        return itemStack.getType().equals(type)
                && !LionsShopUtil.isCounterfeit(itemStack)
                && LionsShopUtil.isEqualIgnoringNBT(itemStack, type, dmg)
                || itemStack.getType().equals(ItemTypes.FILLED_MAP); // Exception for maps.
    }
}