package com.minecraftonline.lionsshop.signs;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.lionsshop.LionsShop;
import com.minecraftonline.lionsshop.LionsShopUtil;
import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.InvalidSignStateException;
import com.minecraftonline.lionsshop.filters.CurrencyFilter;
import com.minecraftonline.lionsshop.filters.StockFilter;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.TileEntityTypes;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class Shop extends LionsShopShopSign {

    private static final String TITLE = "[Shop]";
    private static final ItemStack creationCostItem = ItemStack.of(ItemTypes.DIAMOND);
    private static final int creationCostAmt = 5;

    private boolean legacy = false;
    private boolean hasMachinery = false;

    protected Shop(SignData data, Location<World> signLoc) throws InvalidShopException {
        super(data, signLoc);
        if (SignUtil.getTextRaw(data, 1).equals("-1")) {
            hasMachinery = true;
        }
        if (!SignUtil.getTextRaw(data, 3).isEmpty()) {
            legacy = true;
        }
    }

    public static class Factory extends LionsShopShopSign.Factory<Shop> {

        @Override
        public Shop onCreate(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException {
            Inventory playerInventory = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));
            // create the shop before taking the money, so we don't take the money before the shop is successfully created
            Shop shop = new Shop(data, signLoc);
            Inventory creationCostInv = playerInventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(new CurrencyFilter(creationCostItem.getType(), LionsShopUtil.getDamage(creationCostItem))));
            if (creationCostInv.totalItems() < creationCostAmt) {
                throw new InsufficientCreationCost("You do not have the creation cost (" + creationCostAmt + " " + creationCostItem.getType().getName().split(":")[1] + ")");
            }
            creationCostInv.poll(creationCostAmt);
            player.sendMessage(Text.of(TextColors.GREEN, "Created " + getTitle() + " sign"));
            return shop;
        }

        @Override
        public Shop onLoad(Sign sign) throws InvalidSignCreationException, InvalidSignStateException {
            return new Shop(sign.getSignData(), sign.getLocation());
        }

        @Override
        public String getTitle() {
            return Shop.TITLE;
        }

        @Override
        public String getCreatePermission() {
            return "lionsshop.shop.create";
        }

        static boolean isShopSign(Location loc) {
            if (!loc.getBlockType().equals(BlockTypes.WALL_SIGN)) {
                return false;
            }
            Sign shopSign = (Sign)loc.getTileEntity().get();
            return new Shop.Factory().matches(shopSign.getSignData());
        }
    }

    @Override
    public void onRightClick(Player player) {
        if (legacy) {
            if (SignUtil.getTextRaw(data, 3).equals(player.getName())) {
                migrate(player.getUniqueId());
                setLine(signLoc, Text.of(""), 3);
                player.sendMessage(Text.of(TextColors.GREEN, "Automatically upgraded sign"));
            }
        }
        if (incomplete) {
            player.sendMessage(Text.of(TextColors.RED, "Shop is incomplete, if you are the owner, use /lseditsign to complete it"));
            return;
        }

        Optional<Inventory> optChestInventory = getChest();
        if (!optChestInventory.isPresent()) {
            player.sendMessage(Text.of(TextColors.RED, "Vendor does not have a chest!"));
            return;
        }
        int buyDmg = 0;
        Inventory stockInventory = null;
        Inventory chestInventory = optChestInventory.get();
        if (!hasMachinery) {
            buyDmg = IDUtil.getDamageFromItemStack(buyItem);
            stockInventory = chestInventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(new StockFilter(buyItem.getType(), buyDmg)));
            if (stockInventory.totalItems() < buyAmt) {
                player.sendMessage(Text.of(TextColors.RED, "Vendor is out of stock!"));
                return;
            }
        }
        if (!canFitPayment(chestInventory)) {
            player.sendMessage(Text.of(TextColors.RED, "Vendor cannot fit their payment!"));
            return;
        }

        final int currencyDmg = IDUtil.getDamageFromItemStack(currencyItem);
        Inventory playerInventory = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));

        Inventory currencyInv = playerInventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(new CurrencyFilter(currencyItem.getType(), currencyDmg)));
        if (currencyInv.totalItems() < currencyItemAmt) {
            player.sendMessage(Text.of(TextColors.RED, "You do not have enough currency (Counterfeit currency not accepted!)"));
            return;
        }
        // Please note will only check for up to a stack, and since they can have different NBT etc etc, very possible for
        // items to land on the ground, but at least they will not be able to keep clicking the sign and dump items all over
        // the floor
        if (!hasMachinery && !playerInventory.canFit(ItemStack.builder().from(buyItem).quantity(buyAmt).build())) {
            player.sendMessage(Text.of(TextColors.RED, "You do not have enough space in your inventory!"));
            return;
        }

        if (hasMachinery) {
            Location<World> buttonLoc = signLoc.getRelative(Direction.DOWN);
            try {
                if (isMachineryActive(buttonLoc)) {
                    player.sendMessage(Text.of(TextColors.RED, "Machinery already active, please wait!"));
                    return;
                }
            } catch (LionsShopShopSign.NoMachineryException e) {
                player.sendMessage(Text.of(TextColors.RED, "This shop has no machinery attached!"));
                return;
            }
            doPayment(currencyInv);
            activateMachinery(buttonLoc);
            player.sendMessage(Text.of(TextColors.GREEN, "Activated machinery"));
        }
        else {
            doPayment(currencyInv);
            List<ItemStack> takenItems;
            try {
                takenItems = LionsShopUtil.takeItemsWithDamage(stockInventory, buyItem.getType(), buyDmg, buyAmt);
            } catch (LionsShopUtil.OutOfItemsException e) {
                LionsShop.getLogger().error("Transaction failed badly, ran out of items - should have been checked beforehand. Hopefully all items are preserved but may have been overpaid, at sign at: " + signLoc);
                takenItems = e.getPreservedItems();
            }
            LionsShopUtil.offerItemStacks(takenItems, playerInventory, new LionsShopUtil.RejectedItemsHandler(player));
            player.sendMessage(Text.of("Completed transaction"));
        }
        updateContent();
    }

    @Override
    public Optional<Map.Entry<ItemStack, Integer>> parseAdditional(int lineNum) {
        if (lineNum == 1 && SignUtil.getTextRaw(data, lineNum).equals("-1")) {
            //hasMachinery = true;
            return Optional.of(new AbstractMap.SimpleEntry<>(ItemStack.of(ItemTypes.AIR), 0));
        }
        return Optional.empty();
    }

    private void doPayment(Inventory source) {
        source.poll(currencyItemAmt);
        LionsShopUtil.offerItemStacks(currencyItem, currencyItemAmt, getChest().get());
    }

    private void updateContent() {
        Location<World> contentLoc = signLoc.add(ShopContent.fromShopSign);
        if (!contentLoc.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            return;
        }
        Sign sign = (Sign)contentLoc.getTileEntity().get();
        ShopContent.Factory factory = new ShopContent.Factory();
        if (!factory.matches(sign.getSignData())) {
            return;
        }
        factory.load(sign).update(this);
    }

    private void activateMachinery(Location<World> buttonLoc) {
        BlockType type = buttonLoc.getBlockType();
        buttonLoc.offer(Keys.POWERED, true);
        int delay = 0;
        if (type.equals(BlockTypes.WOODEN_BUTTON)) {
            delay = 30;
        }
        else if (type.equals(BlockTypes.STONE_BUTTON)) {
            delay = 20;
        }

        Task.builder().delayTicks(delay).execute(() -> {
            // Check if block hasn't changed
            if (buttonLoc.getBlock().getType().equals(type)) {
                buttonLoc.offer(Keys.POWERED, false);
            }
        }).submit(LionsShop.getInstance());
    }

    private boolean isMachineryActive(Location<World> buttonLoc) throws LionsShopShopSign.NoMachineryException {
        BlockType type = buttonLoc.getBlockType();
        if (!(type.equals(BlockTypes.WOODEN_BUTTON)
                || type.equals(BlockTypes.STONE_BUTTON))) {
            throw new LionsShopShopSign.NoMachineryException();
        }
        return (boolean)buttonLoc.require(Keys.POWERED);
    }

    private void migrate(UUID uuid) {
        signLoc.getExtent().setCreator(signLoc.getBlockPosition(), uuid);
    }

    Optional<Inventory> getChest() {
        Optional<TileEntity> optChest;
        for (int i = 1; i <= 5; i++)
        {
            optChest = signLoc.add(0, -i, 0).getTileEntity();
            if (optChest.isPresent() && optChest.get().getType().equals(TileEntityTypes.CHEST))
            {
                Chest chest = (Chest)optChest.get();
                return Optional.of(chest.getDoubleChestInventory().orElse(chest.getInventory()));
            }
        }
        return Optional.empty();
    }

    private boolean canFitPayment(Inventory chest) {
        int amtRemaining = currencyItemAmt;
        int maxStackSize = currencyItem.getMaxStackQuantity();
        for (Inventory slot : chest.slots()) {
            Optional<ItemStack> itemStack = slot.peek();
            int quantity;

            if (itemStack.isPresent()) {
                quantity = itemStack.get().getQuantity();
                if (quantity == maxStackSize)
                    continue;
                if (!itemStack.get().isEmpty()) {
                    ItemStack copy = itemStack.get().copy();
                    copy.setQuantity(1);
                    if (!copy.equalTo(currencyItem))
                        continue;
                }
            }
            else {
                quantity = 0;
            }
            // If we reach here we can put some of buyItem into the inventory
            int space = maxStackSize - quantity;
            amtRemaining -= space;
            if (amtRemaining <= 0)
                return true;
        }
        return false;
    }

    private static void setLine(Location<World> signLoc, Text text, int line) {
        List<Text> lines = signLoc.require(Keys.SIGN_LINES);
        lines.set(line, text);
        signLoc.offer(Keys.SIGN_LINES, lines);
    }
}
