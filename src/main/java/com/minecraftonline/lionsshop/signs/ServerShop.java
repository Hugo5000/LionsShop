package com.minecraftonline.lionsshop.signs;

import com.minecraftonline.lionsshop.LionsShopUtil;
import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.filters.CurrencyFilter;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.persistence.DataFormats;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Optional;

public class ServerShop extends LionsShopShopSign {
    public static final String TITLE = "[ServerShop]";


    protected ServerShop(SignData data, Location<World> signLoc) throws InvalidShopException {
        super(data, signLoc);
    }

    public static class Factory extends LionsShopShopSign.Factory<ServerShop> {

        @NonNull
        @Override
        public ServerShop onCreate(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException {
            player.sendMessage(Text.of(TextColors.GREEN, "Created " + getTitle() + " sign"));
            return new ServerShop(data, signLoc);
        }

        @Override
        public ServerShop onLoad(Sign sign) throws InvalidShopException {
            return new ServerShop(sign.getSignData(), sign.getLocation());
        }

        public static boolean isShopSign(Location<World> loc) {
            if (!loc.getBlockType().equals(BlockTypes.WALL_SIGN)) {
                return false;
            }
            Sign shopSign = (Sign)loc.getTileEntity().get();
            return new ServerShop.Factory().matches(shopSign.getSignData());
        }

        @Override
        public String getTitle() {
            return ServerShop.TITLE;
        }

        @Override
        public String getCreatePermission() {
            return "lionsshop.servershop.create";
        }
    }

    @Override
    public void onRightClick(Player player) {
        if (incomplete) {
            player.sendMessage(Text.of(TextColors.RED, "Shop is incomplete, if you are the owner, use /lsshopedit to complete it"));
            return;
        }
        Inventory playerInventory = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));
        Inventory currencyInv = playerInventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(new CurrencyFilter(currencyItem.getType(), LionsShopUtil.getDamage(currencyItem))));
        if (currencyInv.totalItems() < currencyItemAmt) {
            player.sendMessage(Text.of(TextColors.RED, "You do not have enough currency (Counterfeit currency not accepted!)"));
            return;
        }
        // Please note will only check for up to a stack, and since they can have different NBT etc etc, very possible for
        // items to land on the ground, but at least they will not be able to keep clicking the sign and dump items all over
        // the floor
        if (!playerInventory.canFit(ItemStack.builder().from(buyItem).quantity(buyAmt).build())) {
            player.sendMessage(Text.of(TextColors.RED, "You do not have enough space in your inventory!"));
            return;
        }
        currencyInv.poll(currencyItemAmt);
        LionsShopUtil.offerItemStacks(
                LionsShopUtil.createItemStacks(buyItem, buyAmt),
                playerInventory,
                new LionsShopUtil.RejectedItemsHandler(player));
    }

    @Override
    public Optional<Map.Entry<ItemStack, Integer>> parseAdditional(int lineNum) throws InvalidShopException {
        if (lineNum == 1) {
            // NBT support
            String line = SignUtil.getTextRaw(data, lineNum);
            String[] parts = line.split(" ", 4);
            if (parts.length != 4) {
                return Optional.empty();
            }
            Map.Entry<ItemStack, Integer> entry = parseAmountAndDamage(lineNum, parts[0], parts[1], parts[2]);
            String nbt = parts[3];
            if (!(nbt.startsWith("{") && nbt.endsWith("}"))) {
                throw new InvalidShopException("Expected nbt as 4th param on line " + lineNum + ", but it didn't look like nbt", signLoc);
            }

            DataContainer container;
            try {
                container = DataFormats.HOCON.read(nbt);
                //container = DataFormats.NBT.readFrom(stream);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            //LionsShop.getLogger().info(container.toString());
            ItemStack stack = ItemStack.builder().fromContainer(entry.getKey().toContainer().set(DataQuery.of("UnsafeData"), container)).build();
            //LionsShop.getLogger().info("Item: " + stack.toContainer().toString());

            return Optional.of(new AbstractMap.SimpleEntry<>(stack, entry.getValue()));
        }
        return Optional.empty();
    }
}
