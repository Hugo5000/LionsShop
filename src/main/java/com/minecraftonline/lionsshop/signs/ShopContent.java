package com.minecraftonline.lionsshop.signs;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.lionsshop.LionsShopSign;
import com.minecraftonline.lionsshop.LionsShopUtil;
import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.NoShopSignException;
import com.minecraftonline.lionsshop.filters.CurrencyFilter;
import com.minecraftonline.lionsshop.filters.StockFilter;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ShopContent implements LionsShopSign {
    private Location signLoc;
    private SignData data;
    private static final String TITLE = "[ShopContent]";
    static final Vector3i fromShopSign = new Vector3i(0,-1,0);
    private static final Vector3i toShopSign = fromShopSign.mul(-1);

    private ShopContent(Location signLoc, SignData data) {
        this.signLoc = signLoc;
        this.data = data;
    }

    void update(Shop shop) {

        String[] strings = getStringAmounts(shop);

        List<Text> lines = data.lines().get();
        lines.set(1, Text.of(strings[0]));
        lines.set(2, Text.of(strings[1]));
        lines.set(3, Text.of(strings[2]));
        signLoc.offer(Keys.SIGN_LINES, lines);
    }

    private void update(String[] stringAmounts) {
        List<Text> lines = data.lines().get();
        lines.set(1, Text.of(stringAmounts[0]));
        lines.set(2, Text.of(stringAmounts[1]));
        lines.set(3, Text.of(stringAmounts[2]));
        signLoc.offer(Keys.SIGN_LINES, lines);
    }

    private String[] getStringAmounts(Shop shop) {
        Optional<Inventory> chestInv = shop.getChest();
        int freeSpace;
        int stock;
        int totalCurrency;
        Map.Entry<ItemStack, Integer> buyItems = shop.getExampleBuyItems();
        ItemStack buyItem = buyItems.getKey();

        Map.Entry<ItemStack, Integer> currencyItems = shop.getCurrencyItems();
        ItemStack currencyItem = currencyItems.getKey();

        if (!chestInv.isPresent()) {
            freeSpace = 0;
            stock = 0;
            totalCurrency = 0;
        }
        else {
            Inventory stockInv = chestInv.get().query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(
                    new StockFilter(buyItem.getType(), IDUtil.getDamageFromItemStack(buyItem))
            ));
            Inventory currencyInv = chestInv.get().query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(
                    new CurrencyFilter(currencyItem.getType(), IDUtil.getDamageFromItemStack(currencyItem))
            ));
            stock = stockInv.totalItems();
            totalCurrency = currencyInv.totalItems();
            // Amount of slots with air in
            int freeSlots = chestInv.get().capacity() - stockInv.size() - currencyInv.size();
            int currencyMaxStackSize = currencyItem.getMaxStackQuantity();
            // How much can be put in without taking up another slot (eg. 60 can take 4 more if max stack size is 64)
            int roomInStacks = currencyMaxStackSize - (totalCurrency % currencyMaxStackSize);
            freeSpace = (freeSlots * currencyMaxStackSize) + roomInStacks;
        }
        return new String[] {
                LionsShopUtil.getHumanName(buyItem) + ":" + stock,
                LionsShopUtil.getHumanName(currencyItem) + ":" + totalCurrency,
                "Free:" + freeSpace
        };
    }

    @Override
    public void onRightClick(Player player) {
        Location shopSignLoc = signLoc.add(toShopSign);
        if (!Shop.Factory.isShopSign(shopSignLoc)) {
            player.sendMessage(Text.of(TextColors.RED, "No shop above this sign!"));
            return;
        }

        Shop shop = new Shop.Factory().load((Sign)shopSignLoc.getTileEntity().get());

        String[] strings = getStringAmounts(shop);
        update(strings);
        player.sendMessage(
                Text.builder().color(TextColors.GREEN)
                        .append(Text.of(strings[0])).append(Text.NEW_LINE)
                        .append(Text.of(strings[1])).append(Text.NEW_LINE)
                        .append(Text.of(strings[2]))
                        .build());
    }

    public static class Factory implements LionsShopSign.Factory<ShopContent> {
        @Override
        public @NonNull ShopContent create(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException {
            if (!matches(data)) {
                throw new RuntimeException("Sign passed into ShopContent create was not a shopcontent sign! at location: " + signLoc);
            }
            Location shopSignLoc = signLoc.add(toShopSign);
            if (!Shop.Factory.isShopSign(shopSignLoc)) {
                throw new NoShopSignException("ShopContent sign must be directly below a shop sign.");
            }
            // Makes sure that the shop sign is valid
            new Shop.Factory().load((Sign)shopSignLoc.getTileEntity().get());

            return new ShopContent(signLoc, data);
        }

        @Override
        public @NonNull ShopContent load(Sign sign) {
            try {
                return create(sign.getLocation(), sign.getSignData(), null);
            } catch (InvalidSignCreationException e) {
                throw new RuntimeException("Tried to load shop content sign without a shop sign above it, did you mean to use create()?",e);
            }
        }

        @Override
        public boolean matches(SignData signData) {
            return SignUtil.getTextRaw(signData, 0).equalsIgnoreCase(TITLE);
        }

        @Override
        public boolean isInteractable() {
            return true;
        }
    }
}