package com.minecraftonline.lionsshop.signs;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.lionsshop.LionsShopSign;
import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.InvalidSignStateException;
import com.minecraftonline.lionsshop.exceptions.NoShopSignException;
import com.minecraftonline.lionsshop.filters.StockFilter;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.value.mutable.ListValue;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ShopInfo implements LionsShopSign {
    private static final Vector3i fromShopSignVector = new Vector3i(0,1,0);
    private static final Vector3i toShopSignVector = fromShopSignVector.mul(-1);

    private LionsShopShopSign shop;
    private static final String TITLE = "[ShopInfo]";

    private ShopInfo(LionsShopShopSign shop) {this.shop = shop;}

    @Override
    public void onRightClick(Player player) {
        Map.Entry<ItemStack, Integer> buyItems = shop.getExampleBuyItems();
        Map.Entry<ItemStack, Integer> sellItems = shop.getCurrencyItems();

        Optional<ItemStack> realBuyItem = getFirstItem();

        ItemStack buyItem = realBuyItem.orElseGet(buyItems::getKey);
        int buyAmt = buyItems.getValue();

        ItemStack sellItem = sellItems.getKey();
        int sellAmt = sellItems.getValue();

        player.sendMessage(Text.builder("This shop sells: " + buyAmt + " ").color(TextColors.BLUE)
                .append(Text.builder(buyItem.getType().getName().split(":")[1]).color(TextColors.YELLOW).onHover(TextActions.showItem(buyItem.createSnapshot())).build())
                .append(Text.builder(" for " + sellAmt).color(TextColors.BLUE).build())
                .append(Text.builder(" " + sellItem.getType().getName().split(":")[1]).color(TextColors.YELLOW).onHover(TextActions.showItem(sellItem.createSnapshot())).build()).build());

        if (!realBuyItem.isPresent())
            player.sendMessage(Text.of(TextColors.RED, "But is currently out of stock."));
    }

    private Optional<ItemStack> getFirstItem() {
        ItemStack itemStack = shop.getExampleBuyItems().getKey();
        ItemType type = itemStack.getType();
        int dmg = IDUtil.getDamageFromItemStack(itemStack);
        if (!(shop instanceof Shop)) {
            if (shop instanceof ServerShop) {
                return Optional.of(shop.buyItem);
            }
            return Optional.empty();
        }

        Shop regularShop = (Shop) shop;

        Optional<Inventory> inv = regularShop.getChest();
        return inv.flatMap(inventory -> inventory
                .query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(new StockFilter(type, dmg)))
                .peek(1));

    }

    private static List<Text> createSignLines(LionsShopShopSign shop, SignData data) {
        List<Text> lines = data.lines().get();
        lines.set(1, format(shop.getExampleBuyItems()));
        lines.set(2, Text.of("for"));
        lines.set(3, format(shop.getCurrencyItems()));
        return lines;
    }

    private static LionsShopShopSign getShop(Location<World> signLoc) throws NoShopSignException {
        Location<World> shopSignLoc = signLoc.add(toShopSignVector);

        if (Shop.Factory.isShopSign(shopSignLoc)) {
            Sign sign = (Sign)shopSignLoc.getTileEntity().get();
            return new Shop.Factory().load(sign);
        }
        if (ServerShop.Factory.isShopSign(shopSignLoc)) {
            Sign sign = (Sign)shopSignLoc.getTileEntity().get();
            return new ServerShop.Factory().load(sign);
        }
        throw new NoShopSignException("ShopInfo sign must be placed directly above a shop sign");
    }

    private static Text format(Map.Entry<ItemStack, Integer> item) {
        return Text.of(item.getValue() + " " + item.getKey().getType().getName().split(":")[1]);
    }

    public static class Factory implements LionsShopSign.Factory<ShopInfo> {
        @NonNull
        @Override
        public ShopInfo create(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException {
            if (!matches(data))
                throw new RuntimeException("Sign passes into ShopInfo create was not a shopinfo sign! at location: " + signLoc);

            if (player != null && !SignUtil.getTextRaw(data, 0).equals(TITLE)) {
                // Fix casing if it is being created
                data.setElement(0, Text.of(TITLE));
            }
            LionsShopShopSign shop = getShop(signLoc);
            List<Text> lines = createSignLines(shop, data);
            data.setElements(lines);
            return new ShopInfo(shop);
        }

        @NonNull
        @Override
        public ShopInfo load(Sign sign) throws InvalidSignStateException {
            try {
                if (!matches(sign.getSignData()))
                    throw new RuntimeException("Sign passes into ShopInfo load was not a shopinfo sign! at location: " + sign.getLocation());
                LionsShopShopSign shop = getShop(sign.getLocation());
                List<Text> lines = createSignLines(shop, sign.getSignData());
                sign.offer(Keys.SIGN_LINES, lines);
                return new ShopInfo(shop);
            } catch (NoShopSignException e) {
                ListValue<Text> lines = sign.lines();
                lines.set(0, Text.EMPTY);
                lines.set(1, Text.EMPTY);
                lines.set(2, Text.EMPTY);
                lines.set(3, Text.EMPTY);
                sign.offer(lines);
                throw new InvalidSignStateException("ShopInfo sign no longer has a Shop below it, blanking sign.");
            }
        }

        @Override
        public boolean matches(SignData signData) {
            return SignUtil.getTextRaw(signData, 0).equalsIgnoreCase(TITLE);
        }

        @Override
        public boolean isInteractable() {
            return true;
        }
    }
}