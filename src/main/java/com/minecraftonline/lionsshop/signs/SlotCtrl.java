package com.minecraftonline.lionsshop.signs;

import com.minecraftonline.lionsshop.LionsShop;
import com.minecraftonline.lionsshop.LionsShopSign;
import com.minecraftonline.lionsshop.LionsShopUtil;
import com.minecraftonline.lionsshop.SlotRolls;
import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.InvalidSignStateException;
import com.minecraftonline.lionsshop.filters.StockFilter;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.TileEntityTypes;
import org.spongepowered.api.block.tileentity.carrier.Chest;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.*;

public class SlotCtrl implements LionsShopSign {
    private static final String TITLE = "[SlotCtrl]";
    private static final int MAX_PRIZE = 64;
    private static final ItemStack CREATION_COST = ItemStack.of(ItemTypes.DIAMOND, 16);

    private Location<World> location;
    private Direction facing;
    private boolean randomDraw = false;
    private boolean legacy;
    private ItemStack item;
    private Integer multiplier;
    // Can actually be any location that supports Keys.DYE_COLOR
    private List<Location<World>> woolLocations = null;

    private SlotCtrl(Location<World> location, SignData data, Player creator, boolean legacy) throws InvalidSignCreationException {
        this.legacy = legacy;
        List<Text> lines = data.lines().get();
        String line2 = lines.get(1).toPlain();
        if (line2.isEmpty()) {
            throw new InvalidSignCreationException("Line 2 must not be empty. Should contain item and optionally damage");
        }
        String[] line2Split = line2.split(" ");

        if (line2Split.length > 2)
            throw new InvalidSignCreationException("Too many parameters in line 2, expected item and optionally, damage");

        int dmg = 0;
        if (line2Split.length == 2) {
            try {
                dmg = Integer.parseInt(line2Split[1]);
            } catch (NumberFormatException e) {
                throw new InvalidSignCreationException("Damage specified on line 2 is not a number");
            }
        }
        String line3 = lines.get(2).toPlain();
        multiplier = 1;
        if (!line3.isEmpty()) {
            String[] line3Split = line3.split(" ");
            if (line3Split.length > 2) {
                throw new InvalidSignCreationException("Too many parameters, at most expected amount and random flag (r)");
            }
            if (line3Split[0].equals("r")) {
                randomDraw = true;
            }
            else {
                try {
                    multiplier = Integer.parseInt(line3Split[0]);
                } catch (NumberFormatException e) {
                    throw new InvalidSignCreationException("Amount specified on line 2 is not a number");
                }
                if (multiplier <= 0)
                    throw new InvalidSignCreationException("Amount must be greater than 0");
                if (line3Split.length == 2) {
                    if (!line3Split[1].equals("r")) {
                        throw new InvalidSignCreationException("Second parameter must be the random flag 'r'");
                    }
                    randomDraw = true;
                }
            }
        }

        Optional<ItemStack> itemStack = IDUtil.getItemStackFromAnyIDAndDamage(line2Split[0], dmg);
        if (!itemStack.isPresent()) {
            throw new InvalidSignCreationException("Item specified on line 2 is not a valid item");
        }
        item = itemStack.get();
        this.location = location;
        this.facing = location.require(Keys.DIRECTION);
        if (!isStructureValid()) {
            throw new NoWoolException("Dye-able blocks are not in the correct configuration!");
        }
        if (creator != null) {
            Inventory creationInv = creator.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class))
                    .query(QueryOperationTypes.ITEM_STACK_IGNORE_QUANTITY.of(CREATION_COST));
            if (creationInv.totalItems() < CREATION_COST.getQuantity()) {
                throw new InvalidSignCreationException("You do not have the creation cost (" + CREATION_COST.getQuantity() + " " + LionsShopUtil.getHumanName(CREATION_COST) + ")");
            }
            creationInv.poll(CREATION_COST.getQuantity());
        }
    }

    private boolean isStructureValid() {
        for (Location<World> loc : getWoolLocations()) {
            if (!(loc.supports(Keys.DYE_COLOR))) {
                return false;
            }
        }
        return true;
    }

    private List<Location<World>> getWoolLocations() {
        if (woolLocations != null)
            return woolLocations;
        Direction leftRight; // Either NORTH or EAST
        if (facing.equals(Direction.NORTH) || facing.equals(Direction.SOUTH)) {
            leftRight = Direction.EAST;
        }
        else {
            leftRight = Direction.NORTH;
        }
        Location<World> middleWool = location.add(facing.getOpposite().asBlockOffset()).add(0,1,0);
        woolLocations = new ArrayList<>();
        woolLocations.add(middleWool.add(leftRight.asBlockOffset()));
        woolLocations.add(middleWool);
        woolLocations.add(middleWool.add(leftRight.getOpposite().asBlockOffset()));
        return woolLocations;
    }

    @Override
    public void onRightClick(Player player) {
        if (legacy) {
            if (player.getName().equals(location.require(Keys.SIGN_LINES).get(3).toPlain())) {
                migrate(player.getUniqueId());
                player.sendMessage(Text.of(TextColors.GREEN,"Successfully updated slot sign"));
                LionsShop.getLogger().info("Auto upgraded SlotCtrl sign at " + location + " from username: " + player.getName() + " to uuid: " + player.getUniqueId());
            }
            else {
                // Don't be too spammy.
                if (new Random().nextInt(5) == 0) {
                    player.sendMessage(Text.of(TextColors.YELLOW, "Slot is legacy, please contact owner to update it!"));
                }
            }
        }
        Optional<ItemStack> heldItem = player.getItemInHand(HandTypes.MAIN_HAND);
        if (heldItem.isPresent() && heldItem.get().getType().equals(ItemTypes.BOOK)) {
            showProbabilities(player);
            return;
        }

        Inventory playerMainInventory = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));
        StockFilter filter = new StockFilter(item.getType(), IDUtil.getDamageFromItemStack(item));
        Inventory currencyInv = playerMainInventory.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(filter));
        if (currencyInv.totalItems() < multiplier) {
            player.sendMessage(Text.of(TextColors.RED, "You cant play because you can't pay! (Multiplier: " + multiplier + ")"));
            return;
        }
        Optional<Inventory> optChest = getChest();
        if (!optChest.isPresent()) {
            player.sendMessage(Text.of(TextColors.RED, "The casino has no storage!"));
            return;
        }
        Inventory chestInv = optChest.get();
        Inventory stockInv = chestInv.query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(filter));
        if (stockInv.totalItems() < multiplier*MAX_PRIZE) {
            player.sendMessage(Text.of(TextColors.RED, "Casino is bust!"));
            return;
        }
        // Assemble example stacks for player - we dont care too much if some items end up
        // on the floor due to different NBT
        List<ItemStack> exampleWinnings = LionsShopUtil.createItemStacks(item, multiplier*MAX_PRIZE);
        if (!LionsShopUtil.canFitItemStacks(exampleWinnings, playerMainInventory)) {
            player.sendMessage(Text.of(TextColors.RED, "You cannot fit the max prize!"));
            return;
        }

        // Do this last as it is likely most expensive - but not actually if payment is one ItemStack
        // Get real items that will be used by the player and offer them
        // into inventory to see if they fit, then revert changes
        List<ItemStack> payment = LionsShopUtil.getFirst(currencyInv, multiplier, false);
        boolean canCasinoFit;
        if (payment.size() == 1) {
            canCasinoFit = chestInv.canFit(payment.get(0));
        }
        else {
            canCasinoFit = LionsShopUtil.canFitItemStacks(payment, chestInv);
        }
        if (!canCasinoFit) {
            player.sendMessage(Text.of(TextColors.RED, "Casino's bank is full!"));
            return;
        }
        currencyInv.poll(multiplier);
        LionsShopUtil.offerItemStacks(payment, chestInv);

        int[] rolls = SlotRolls.roll();
        int prize = SlotRolls.getPrize(rolls[0], rolls[1], rolls[2]) * multiplier;
        for (int i = 0; i < rolls.length; i++) {
            woolLocations.get(i).offer(Keys.DYE_COLOR, SlotRolls.diceRollToColor(rolls[i]));
        }
        List<ItemStack> winnings;
        if (randomDraw) {
            winnings = getRandomItems(stockInv, prize);
            LionsShop.getLogger().info("winnings size: " + winnings.size());
        }
        else {
            winnings = LionsShopUtil.getFirst(stockInv, prize, true);
        }
        LionsShopUtil.offerItemStacks(winnings, playerMainInventory, new LionsShopUtil.RejectedItemsHandler(player));
        if (prize != 0) {
            player.sendMessage(Text.of(TextColors.GREEN, "You have won: " + prize + " " + LionsShopUtil.getHumanName(item)));
        }
        updateInfo(prize);
    }

    private void updateInfo(int prize) {
        Location<World> slotInfoLoc = location.add(SlotInfo.fromSlotCtrl);
        if (!slotInfoLoc.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            return;
        }
        Sign sign = (Sign)slotInfoLoc.getTileEntity().get();
        SlotInfo.Factory factory = new SlotInfo.Factory();
        if (!factory.matches(sign.getSignData())) {
            return;
        }
        // if no prize update line 2 (losses) else wins
        factory.load(sign).increment(prize == 0 ? 2 : 1);
    }

    private void migrate(UUID uuid) {
        location.getExtent().setCreator(location.getBlockPosition(), uuid);
    }

    private Optional<Inventory> getChest() {
        Location<World> chestLoc = location.add(0,-1,0);
        Optional<TileEntity> optChest = chestLoc.getTileEntity();
        if (optChest.isPresent() && optChest.get().getType().equals(TileEntityTypes.CHEST)) {
            Chest chest = (Chest)optChest.get();
            return Optional.of(chest.getDoubleChestInventory().orElse(chest.getInventory()));
        }
        return Optional.empty();
    }

    private static List<ItemStack> getRandomItems(Inventory inv, int amt) {
        List<ItemStack> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < amt; i++) {
            int r = random.nextInt(inv.totalItems());
            int runningTotal = 0;
            for (Inventory slot : inv.slots()) {
                Optional<ItemStack> itemStack = slot.peek();
                if (!itemStack.isPresent()) {
                    continue;
                }
                int quantity = itemStack.get().getQuantity();
                runningTotal += quantity;
                if (runningTotal >= r) {
                    // Have found stack to get.
                    ItemStack newStack = itemStack.get().copy();
                    newStack.setQuantity(1);
                    slot.poll(1);
                    // If key is present, sum (increment since its 1)
                    list.add(newStack);
                    break;
                }
            }
        }
        return list;
    }

    public static class Factory implements LionsShopSign.Factory<SlotCtrl> {

        @NonNull
        @Override
        public SlotCtrl create(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException {
            if (!matches(data)) {
                throw new RuntimeException("Sign data passed into SlotCtrl.create() was not a SlotCtrl sign");
            }
            if (!player.hasPermission("lionsshop.createslots")) {
                throw new InvalidSignCreationException("You do not have permission to create a SlotCtrl sign!");
            }
            if (!SignUtil.getTextRaw(data, 0).equals(TITLE)) {
                // Fix casing
                data.setElement(0, Text.of(TITLE));
            }
            if (!SignUtil.getTextRaw(data, 3).isEmpty()) {
                data.setElement(3, Text.EMPTY);
            }
            return new SlotCtrl(signLoc, data, player , false);
        }

        @NonNull
        @Override
        public SlotCtrl load(Sign sign) throws InvalidSignStateException {
            try {
                SignData data = sign.getSignData();
                boolean legacy = !SignUtil.getTextRaw(data, 3).isEmpty();
                return new SlotCtrl(sign.getLocation(), data, null, legacy);
            } catch (NoWoolException e) {
                throw new InvalidSignStateException(e.getLocalizedMessage());
            } catch (InvalidSignCreationException e) {
                throw new RuntimeException("SlotCtrl sign is corrupted at: " + sign.getLocation(), e);
            }
        }

        @Override
        public boolean matches(SignData signData) {
            return SignUtil.getTextRaw(signData, 0).equalsIgnoreCase(TITLE);
        }

        @Override
        public boolean isInteractable() {
            return true;
        }
    }

    public static class NoWoolException extends InvalidSignCreationException {
        public NoWoolException(String msg) {
            super(msg);
        }

        public NoWoolException(String msg, Exception e) {
            super(msg, e);
        }
    }

    private void showProbabilities(Player player) {
        player.sendMessage(Text.builder("This slot machine plays with ").color(TextColors.BLUE)
                .append(Text.builder(item.getType().getName().split(":")[1]).color(TextColors.WHITE).build())
                .append(Text.builder(" and a multiplier of ").color(TextColors.BLUE).build())
                .append(Text.builder("" + multiplier).color(TextColors.WHITE).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("Winning plan (colors are approximations):").color(TextColors.BLUE).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("#").color(TextColors.GOLD)
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + multiplier).color(TextColors.WHITE).build())
                .append(Text.builder(" | ").color(TextColors.BLUE).build())
                .append(Text.builder("##").color(TextColors.GOLD).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 2)).color(TextColors.WHITE).build())
                .append(Text.builder(" | ").color(TextColors.BLUE).build())
                .append(Text.builder("##").color(TextColors.GREEN).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 3)).color(TextColors.WHITE).build()).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("###").color(TextColors.GOLD).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 5)).color(TextColors.WHITE).build())
                .append(Text.builder(" | ").color(TextColors.BLUE).build())
                .append(Text.builder("###").color(TextColors.GREEN).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 7)).color(TextColors.WHITE).build())
                .append(Text.builder(" | ").color(TextColors.BLUE).build())
                .append(Text.builder("###").color(TextColors.BLUE).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 10)).color(TextColors.WHITE).build())
                .append(Text.NEW_LINE)
                .append(Text.builder("###").color(TextColors.LIGHT_PURPLE).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 25)).color(TextColors.WHITE).build())
                .append(Text.builder(" | ").color(TextColors.BLUE).build())
                .append(Text.builder("###").color(TextColors.YELLOW).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 40)).color(TextColors.WHITE).build())
                .append(Text.builder(" | ").color(TextColors.BLUE).build())
                .append(Text.builder("###").color(TextColors.RED).build())
                .append(Text.builder(" = ").color(TextColors.WHITE).build())
                .append(Text.builder("" + (multiplier * 64)).color(TextColors.WHITE).build()).build());
    }
}