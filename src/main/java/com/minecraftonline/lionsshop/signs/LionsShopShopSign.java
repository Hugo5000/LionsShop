package com.minecraftonline.lionsshop.signs;

import com.minecraftonline.lionsshop.LionsShopSign;
import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.InvalidSignStateException;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Optional;

public abstract class LionsShopShopSign implements com.minecraftonline.lionsshop.LionsShopSign {
    protected boolean incomplete = false;
    protected Location<World> signLoc;
    protected SignData data;

    // What the shop gives the player (nothing for machinery)
    protected ItemStack buyItem;
    protected int buyAmt;
    // What the player gives the shop
    protected ItemStack currencyItem;
    protected int currencyItemAmt;

    protected LionsShopShopSign(SignData data, Location<World> signLoc) throws InvalidShopException {
        this.data = data;
        this.signLoc = signLoc;
        parseLines();
    }

    public abstract static class Factory<T extends LionsShopShopSign> implements LionsShopSign.Factory<T> {
        @Override
        public boolean isInteractable() {
            return true;
        }

        @Override
        @NonNull
        public T create(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException {
            if (!player.hasPermission(getCreatePermission())) {
                throw new InvalidSignCreationException("You do not have permission to create a " + getTitle() + " sign.");
            }

            String line1 = SignUtil.getTextRaw(data, 0);

            if (!line1.equals(getTitle())) {
                if (matches(data)) {
                    // Fix casing
                    data.setElement(0, Text.of(getTitle()));
                }
                else {
                    throw new RuntimeException("Sign passed into Shop constructor (creator) is not a " + getTitle() + " sign.");
                }
            }
            return onCreate(signLoc, data, player);
        }

        @Override
        public @NonNull T load(Sign sign) {
            if (!matches(sign.getSignData())) {
                throw new RuntimeException("Sign passed into Shop constructor (loader) is not a " + getTitle() + " sign.");
            }
            try {
                return onLoad(sign);
            } catch (InvalidSignCreationException | InvalidSignStateException e) {
                throw new RuntimeException("Corrupted " + getTitle() + " sign, at " + sign.getLocation(), e);
            }
        }

        @Override
        public boolean matches(SignData signData) {
            return SignUtil.getTextRaw(signData, 0).equalsIgnoreCase(getTitle());
        }

        public abstract T onCreate(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException;

        public abstract T onLoad(Sign sign) throws InvalidSignCreationException, InvalidSignStateException;

        public abstract String getTitle();

        public abstract String getCreatePermission();
    }

    private void parseLines() throws InvalidShopException {
        parseLine(1).ifPresent(line -> {
            this.buyItem = line.getKey();
            this.buyAmt = line.getValue();
        });
        parseLine(2).ifPresent(line -> {
            this.currencyItem = line.getKey();
            this.currencyItemAmt = line.getValue();
        });
    }

    private Optional<Map.Entry<ItemStack, Integer>> parseLine(int lineNum) throws InvalidShopException {
        String line = SignUtil.getTextRaw(data, lineNum);
        if (line.isEmpty()) {
            incomplete = true;
            return Optional.empty();
        }
        Optional<Map.Entry<ItemStack, Integer>> opt = parseAdditional(lineNum);
        if (opt.isPresent()) {
            return opt;
        }
        String[] splitLine = line.split(" ");
        if (!(splitLine.length == 2 || splitLine.length == 3)) {
            throw new InvalidShopException("Incorrect amount of parameters, expected 2 or 3: itemid amount (damage value), on line " + lineNum, signLoc);
        }

        String amt = splitLine.length == 3 ? splitLine[2] : null;

        return Optional.of(parseAmountAndDamage(lineNum, splitLine[0], splitLine[1], amt));
    }

    protected Map.Entry<ItemStack, Integer> parseAmountAndDamage(int lineNum, String id, String strAmt, @Nullable String strDmg) throws InvalidShopException {
        int amt;
        int dmg = 0;
        try {
            amt = Integer.parseInt(strAmt);
        } catch (NumberFormatException e) {
            throw new InvalidShopException("Amount was not an number", signLoc);
        }
        if (strDmg != null) {
            try {
                dmg = Integer.parseInt(strDmg);
            } catch (NumberFormatException e) {
                throw new InvalidShopException("Damage value was not an number", signLoc);
            }
        }
        Optional<ItemStack> optItemStack = IDUtil.getItemStackFromAnyIDAndDamage(id, dmg);
        if (!optItemStack.isPresent()) {
            throw new InvalidShopException("Unknown item on line " + lineNum, signLoc);
        }
        return new AbstractMap.SimpleImmutableEntry<>(optItemStack.get(), amt);
    }

    public Map.Entry<ItemStack, Integer> getExampleBuyItems() {
        return new AbstractMap.SimpleImmutableEntry<>(buyItem, buyAmt);
    }

    public Map.Entry<ItemStack, Integer> getCurrencyItems() {
        return new AbstractMap.SimpleImmutableEntry<>(currencyItem, currencyItemAmt);
    }

    /**
     * First optional is whether underlying should be returned immediately
     * @param lineNum
     * @return
     */
    public Optional<Map.Entry<ItemStack, Integer>> parseAdditional(int lineNum) throws InvalidShopException {
        return Optional.empty();
    }

    public static class InvalidShopException extends InvalidSignCreationException {
        Location signLoc;

        InvalidShopException(String msg, Location signLoc) {
            super(msg);
            this.signLoc = signLoc;
        }
        InvalidShopException(String msg, Location signLoc, Exception e) {
            super(msg, e);
            this.signLoc = signLoc;
        }

        public Location getLocation() {
            return signLoc;
        }
    }

    public static class InsufficientCreationCost extends InvalidSignCreationException {
        InsufficientCreationCost(String msg) {
            super(msg);
        }
    }
    public static class NoMachineryException extends Exception {
        NoMachineryException() {}
    }
}
