package com.minecraftonline.lionsshop;

import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.InvalidSignStateException;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.TileEntityTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.List;
import java.util.Optional;

public class lsSignListener {

    private List<LionsShopSign.Factory> signTypes;

    public lsSignListener(List<LionsShopSign.Factory> signTypes) {
        this.signTypes = signTypes;
    }

    @Listener
    public void onChangeSignEvent(ChangeSignEvent event) {
        if (!event.getTargetTile().getBlock().getType().equals(BlockTypes.WALL_SIGN))
            return;
        Sign sign = event.getTargetTile();
        Optional<Player> player = event.getCause().first(Player.class);
        if (!player.isPresent()) {
            LionsShop.getLogger().info("Non-present player right clicked on a sign");
            return;
        }

        for (LionsShopSign.Factory signType : signTypes) {
            if (signType.matches(event.getText())) {
                try {
                    signType.create(sign.getLocation(), event.getText(), player.get());
                } catch (InvalidSignCreationException e) {
                    player.get().sendMessage(Text.of(TextColors.RED, e.getMessage()));
                    event.setCancelled(true);
                }
                return;
            }
        }
    }

    @Listener
    public void onInteract(InteractBlockEvent.Secondary e) {
        Sign sign;
        Optional<Player> player = e.getCause().first(Player.class);
        if (!player.isPresent()) {
            return;
        }
        if (!e.getTargetBlock().getLocation().isPresent()) {
            return;
        }
        if (player.get().get(Keys.IS_SNEAKING).orElse(false)) {
            return;
        }

        Optional<TileEntity> tileEntity = e.getTargetBlock().getLocation().get().getTileEntity();
        if (!tileEntity.isPresent()) {
            // Possible Button Shop Sign
            if (e.getTargetBlock().getState().getType().equals(BlockTypes.WOODEN_BUTTON)
                    || e.getTargetBlock().getState().getType().equals(BlockTypes.STONE_BUTTON)) {
                Optional<TileEntity> optionalTileEntity = e.getTargetBlock().getLocation().get()
                        .add((e.getTargetBlock().getState().get(Keys.DIRECTION).get().getOpposite().asBlockOffset().mul(2))).getTileEntity();
                if (!optionalTileEntity.isPresent() || !optionalTileEntity.get().getType().equals(TileEntityTypes.SIGN))
                    return;
                sign = (Sign) optionalTileEntity.get();
            }
            else {
                return;
            }
        }
        else {
            if (!tileEntity.get().getType().equals(TileEntityTypes.SIGN)) {
                return;
            }
            sign = (Sign) tileEntity.get();

        }

        for (LionsShopSign.Factory signType : signTypes) {
            if (signType.isInteractable() && signType.matches(sign.getSignData())) {
                try {
                    LionsShopSign lionsShopSign = signType.load(sign);
                    e.setCancelled(true);
                    lionsShopSign.onRightClick(player.get());
                } catch (InvalidSignStateException ex) {
                    player.get().sendMessage(Text.of(TextColors.RED, ex.getMessage()));
                }
                return;
            }
        }
    }
}
