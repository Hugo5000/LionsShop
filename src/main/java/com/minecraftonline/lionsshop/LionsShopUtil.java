package com.minecraftonline.lionsshop;

import com.minecraftonline.util.InventoryUtil;
import net.minecraft.entity.item.EntityItem;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;

public class LionsShopUtil {
    public static boolean isCounterfeit(ItemStack itemStack)
    {
        if (itemStack.toContainer().contains(DataQuery.of("UnsafeData", "dirtyItem")))
        {
            if (itemStack.toContainer().get(DataQuery.of("UnsafeData","dirtyItem")).get().equals(1))
                return true;
        }
        return false;
    }

    public static boolean hasNbt(ItemStack itemStack) {
        return itemStack.toContainer().contains(DataQuery.of("UnsafeData"));
    }

    // Assumes the inventory is to be taken out of directly, and is already filtered
    public static LinkedList<ItemStack> GetItemsFromInv(Inventory inv, int amount)
    {
        LinkedList<ItemStack> itemlist = new LinkedList<ItemStack>();
        for (Inventory slot : inv.slots()) {
            Optional<ItemStack> tempItemStack = slot.peek();
            if (amount == 0)
                break;
            if (!tempItemStack.isPresent())
                continue;
            if (isCounterfeit(tempItemStack.get()))
                return null;
            if (amount >= tempItemStack.get().getQuantity()) {
                amount -= tempItemStack.get().getQuantity();
                itemlist.add(tempItemStack.get());
            }
            else {
                itemlist.add(ItemStack.builder().fromItemStack(tempItemStack.get()).quantity(amount).build());
                break;
            }
        }
        return itemlist;
    }

    // Returns a map of different itemstacks with same itemtype, (different nbt etc but not damage)
    public static HashMap<ItemStack, Integer> CountItemTypes(Inventory inv, ItemType itemType, int dmgval)
    {
        boolean foundval = false;
        HashMap<ItemStack, Integer> map = new HashMap<ItemStack, Integer>();
        for (Inventory slot : inv.slots())
        {
            Optional<ItemStack> optionalItemStack = slot.peek();
            if (!optionalItemStack.isPresent())
                continue;
            ItemStack itemStack = optionalItemStack.get().copy();
            if (!(itemStack.getType().equals(itemType) && itemStack.toContainer().get(DataQuery.of("UnsafeDamage")).get().equals(dmgval)))
                continue;
            if (isCounterfeit(itemStack))
                return null;
            int stackSize = itemStack.getQuantity();
            itemStack.setQuantity(1); // Set quantity to 1 so they can be equal to others.
            for (Map.Entry<ItemStack, Integer> entry : map.entrySet())
            {
                if (entry.getKey().equals(itemStack))
                {
                    foundval = true;
                    entry.setValue(entry.getValue() + stackSize);
                    break;
                }
                else if (entry.getKey().toContainer().equals(itemStack.toContainer()))
                {
                    foundval = true;
                    entry.setValue(entry.getValue() + stackSize);
                    break;
                }
            }
            if (!foundval)
            {
                map.put(itemStack, stackSize);
            }
            foundval = false;
        }
        return map;
    }

    /**
     *
     * @param source to take ItemStacks from
     * @param type ItemType to match
     * @param damage to match
     * @param amount to take
     * @return null if ran out of items (should never run out of items), or List of items taken
     */
    public static List<ItemStack> takeItemsWithDamage(Inventory source, ItemType type, int damage, int amount) throws OutOfItemsException {
        List<ItemStack> list = new ArrayList<>();
        for (Inventory slot : source.slots()) {
            Optional<ItemStack> optItemStack = slot.peek();
            if (!optItemStack.isPresent())
                continue;
            ItemStack itemStack = optItemStack.get();
            if (!isEqualIgnoringNBT(itemStack, type, damage)) {
                continue;
            }

            int quantity = itemStack.getQuantity();
            if (quantity >= amount) {
                slot.poll(amount);
                itemStack.setQuantity(amount);
                list.add(itemStack);
                return list;
            }
            else {
                slot.poll();
                amount -= quantity;
                list.add(itemStack);
            }
        }
        throw new OutOfItemsException("Ran out of items in takeItemsWithDamage()", list);
    }

    public static List<ItemStack> getFirst(Inventory inv, int amount, boolean shouldRemove) {
        List<ItemStack> itemStacks = new ArrayList<>();
        for (Inventory slot : inv.slots()) {
            if (amount <= 0) {
                return itemStacks;
            }
            Optional<ItemStack> itemStack;

            itemStack = slot.peek();

            if (!itemStack.isPresent()) {
                continue;
            }
            int quantity = Math.min(amount, itemStack.get().getQuantity());
            if (shouldRemove) {
                slot.poll(quantity);
            }
            amount -= quantity;
            ItemStack newItemStack = itemStack.get().copy();
            newItemStack.setQuantity(quantity);
            itemStacks.add(newItemStack);
        }
        // Ran out of items.
        return itemStacks;
    }

    public static boolean canFitItemStacks(List<ItemStack> itemStacks, Inventory target) {
        int emptySlots = target.capacity() - target.size();
        if (itemStacks.size() <= emptySlots) // ItemStack will always fit in one slot.
            return true;
        List<ItemStack> snapshot = createInventorySnapshot(target);
        for (ItemStack itemStack : itemStacks) {
            if (!itemStack.isEmpty() || !target.offer(itemStack).getRejectedItems().isEmpty()) {
                restoreInventory(target, snapshot);
                return false;
            }
        }
        restoreInventory(target, snapshot);
        return true;
    }

    public static List<ItemStack> createInventorySnapshot(Inventory inv) {
        List<ItemStack> snapshot = new ArrayList<>();
        for (Inventory slot : inv.slots()) {
            Optional<ItemStack> optItemStack = slot.peek();
            if (optItemStack.isPresent()) {
                snapshot.add(optItemStack.get().copy());
            }
            else {
                snapshot.add(ItemStack.empty());
            }
        };
        return snapshot;
    }

    public static void restoreInventory(Inventory inv, List<ItemStack> snapshot) {
        Iterator<Inventory> slots = inv.slots().iterator();
        for (ItemStack itemStack : snapshot) {
            Inventory slot = slots.next();
            slot.poll();
            slot.offer(itemStack);
        }
    }

    public static void offerItemStacks(ItemStack itemStack, int amount, Inventory inventory) {
        int maxStackSize = itemStack.getMaxStackQuantity();
        for (; amount > 0;) {
            ItemStack iStack = itemStack.copy();
            int stackSize = Math.min(maxStackSize, amount);
            iStack.setQuantity(stackSize);
            inventory.offer(iStack);
            amount -= stackSize;
        }
    }

    public static void offerItemStacks(List<ItemStack> itemStacks, Inventory inv) {
        for (ItemStack itemStack : itemStacks) {
            inv.offer(itemStack);
        }
    }

    public static void offerItemStacks(List<ItemStack> itemStacks, Inventory inv, @Nullable Consumer<ItemStackSnapshot> rejectedItemStacks) {
        for (ItemStack itemStack : itemStacks) {
            if (rejectedItemStacks == null) {
                inv.offer(itemStack);
            }
            else {
                InventoryUtil.offerInventory(inv, itemStack, rejected -> {
                    rejectedItemStacks.accept(rejected.createSnapshot());
                });
            }
        }
    }

    public static List<ItemStack> createItemStacks(ItemStack itemStack, int amt) {
        int maxStackSize = itemStack.getMaxStackQuantity();
        List<ItemStack> itemStacks = new ArrayList<>();
        ItemStack newStack;
        for (int quantity = Math.min(amt, maxStackSize); amt > 0;
             amt -= quantity, quantity = Math.min(amt, maxStackSize)) {
            newStack = itemStack.copy();
            newStack.setQuantity(quantity);
            itemStacks.add(newStack);
        }
        return itemStacks;
    }

    // Returns true if type and damage are the same
    public static boolean isEqualIgnoringNBT(ItemStack itemStack1, ItemStack itemStack2) {
        if (!itemStack1.getType().equals(itemStack2.getType()))
            return false;
        return (int)itemStack1.toContainer().get(DataQuery.of("UnsafeDamage")).get()
                == (int)itemStack2.toContainer().get(DataQuery.of("UnsafeDamage")).get();
    }

    public static boolean isEqualIgnoringNBT(ItemStack itemStack1, ItemType type, int damage) {
        if (!itemStack1.getType().equals(type))
            return false;
        return (int)itemStack1.toContainer().get(DataQuery.of("UnsafeDamage")).get()
                == damage;
    }

    public static int getDamage(ItemStack itemStack) {
        return (int)itemStack.toContainer().get(DataQuery.of("UnsafeDamage")).get();
    }

    public static void setOwner(Entity item, Player player)
    {
        EntityItem entityItem = (EntityItem) item;
        entityItem.setOwner(player.getName());
    }

    public static String getHumanName(ItemStack itemStack) {
        return getHumanName(itemStack.getType());
    }
    public static String getHumanName(ItemType type) {
        return type.getName().split(":")[1];
    }

    public static class RejectedItemsHandler implements Consumer<ItemStackSnapshot> {
        private Player player;

        public RejectedItemsHandler(Player player) {
            this.player = player;
        }

        @Override
        public void accept(ItemStackSnapshot itemStack) {
            Location<World> playerLoc = player.getLocation();
            Entity item = playerLoc.getExtent().createEntity(EntityTypes.ITEM, playerLoc.getPosition());
            item.offer(Keys.REPRESENTED_ITEM, itemStack);
            LionsShopUtil.setOwner(item, player);
            playerLoc.getExtent().spawnEntity(item);
        }
    }

    public static class OutOfItemsException extends Exception {
        List<ItemStack> preservedItems;
        public OutOfItemsException(String msg, List<ItemStack> preservedItems) {
            super(msg);
            this.preservedItems = preservedItems;
        }

        public List<ItemStack> getPreservedItems() {
            return preservedItems;
        }
    }
}
