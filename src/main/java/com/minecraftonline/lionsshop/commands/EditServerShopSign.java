package com.minecraftonline.lionsshop.commands;

import com.minecraftonline.lionsshop.LionsShop;
import com.minecraftonline.lionsshop.LionsShopUtil;
import com.minecraftonline.lionsshop.signs.ServerShop;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.prompt.ItemStackSnapshotDataPrompt;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.persistence.DataFormats;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;

import java.io.IOException;
import java.util.Optional;

public class EditServerShopSign implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        Optional<String> buySell = args.getOne(Text.of("buysell"));

        Optional<Integer> item = args.getOne(Text.of("item"));

        Optional<Integer> dmgArg = args.getOne(Text.of("damage"));

        int amount = (int) args.getOne("amount").get();

        if (!(amount > 0))
            throw new CommandException(Text.of("Amount must be above 0"));
        if (amount > 2304) {
            throw new CommandException(Text.of("Amount must be less than or equal to 2304"));
        }

        if (!(src instanceof Player))
        {
            throw new CommandException(Text.of("Must be called from a player"));
        }
        Player player = (Player) src;

        BlockRay<World> blockRay = BlockRay.from(player).build();
        BlockRayHit<World> hitOpt = blockRay.next();
        for (int i = 0; i < 9; i++) // number of blocks to check for a sign, (-1 off total since next() is called the line before
        {
            if (hitOpt.getLocation().getBlockType().equals(BlockTypes.WALL_SIGN))
                break;
            hitOpt = blockRay.next();
        }
        if (!hitOpt.getLocation().getBlockType().equals(BlockTypes.WALL_SIGN))
        {
            throw new CommandException(Text.of("No sign found after search"));
        }

        Sign sign = (Sign)hitOpt.getLocation().getTileEntity().get();

        if (!sign.lines().get(0).toPlain().equalsIgnoreCase(ServerShop.TITLE))
        {
            throw new CommandException(Text.of("Sign is not a server shop sign"));
        }

        if (item.isPresent()) {
            if (!dmgArg.isPresent()) {
                throw new CommandException(Text.of("If you provide a item argument you must provide a damage argument!"));
            }
            int itemid = item.get();

            String s = itemid + " " + amount;
            if (dmgArg.get() != 0) {
                s = s + " " + dmgArg.get();
            }

            int line = buySell.get().equals("buy") ? 1 : 2;

            sign.offer(sign.lines().set(line, Text.of(s)));

            return CommandResult.success();
        }

        ItemStackSnapshotDataPrompt dataPrompt = new ItemStackSnapshotDataPrompt(1, 1, "Enter Items");

        dataPrompt.getData(player, callback ->
        {
            Entity summonItem;
            Extent extent = player.getLocation().getExtent();
            for (ItemStackSnapshot itemSnapshot : callback)
            {
                summonItem = extent.createEntity(EntityTypes.ITEM, player.getLocation().getPosition());
                summonItem.offer(Keys.REPRESENTED_ITEM, itemSnapshot);

                LionsShopUtil.setOwner(summonItem, player);

                player.getLocation().spawnEntity(summonItem);
            }
            if (callback.size() < 1) {
                player.sendMessage(Text.of(TextColors.RED, "You must provide at least one item!"));
                return;
            }
            ItemStackSnapshot itemStack = callback.get(0);
            int dmgval = (int) itemStack.toContainer().get(DataQuery.of("UnsafeDamage")).get();
            if (itemStack.getType().equals(ItemTypes.FILLED_MAP))
                dmgval = 0; // dont include dmg value for maps

            final int line;

            int legacyId = IDUtil.getLegacyIDFromItemStack(itemStack.getType());
            String s = legacyId + " " + amount;
            if (dmgval != 0) {
                s = s + " " + dmgval;
            }

            if (buySell.get().equals("buy")) {

                line = 1;
                Optional<DataView> nbt = itemStack.toContainer().getView(DataQuery.of("UnsafeData"));
                if (nbt.isPresent()) {
                    try {
                        if (dmgval == 0) {
                            // Wasn't added before, but we're adding an arg after so we must add it as 0.
                            s = s + " " + dmgval;
                        }
                        s = s + " " + DataFormats.JSON.write(nbt.get());
                    } catch (IOException e) {
                        throw new IllegalStateException(e);
                    }
                }
            }
            else {
                line = 2;
            }
            sign.offer(sign.lines().set(line, Text.of(s)));
        });

        return CommandResult.success();
    }
}
